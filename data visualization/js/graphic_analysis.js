/**
 * Created with JetBrains WebStorm.
 * User: rick
 * Date: 2013-05-03
 * Time: 13:07
 * To change this template use File | Settings | File Templates.
 */
angular.module('graphic_analysis', [])
    .controller('SequenceController', ['$scope', '$http', function ($scope, $http) {
        console.log('initing sequence controller');
        $scope.activeSequences = [{name:'mSeq',pulses:4,split:2}, {name:'altSeqA',pulses:5,split:3}, {name:'altSeqB'}, {name:'altSeqC'}];
        $scope.selectedSequence = $scope.activeSequences[0];
        $scope.sequenceList = null;
        $scope.sliderIncrement = 0.05;
        $scope.createSequence = function() {
            var sequence = create_canonic_reversal_pattern($scope.selectedSequence.pulses, $scope.selectedSequence.split, 'main_seq', $scope.selectedSequence.name);
            $scope.sequenceList = sequence.dump();
            console.log('sequence created:', $scope.sequenceList);
            $scope.$broadcast('sequenceInitialized', $scope.selectedSequence);

      }
        $scope.change = function() {
            console.log('change');
            create_canonic_reversal_pattern($scope.selectedSequence.pulses, $scope.selectedSequence.split, 'main_seq', $scope.selectedSequence.name);
            $scope.sequenceList = sequence.dump();
            $scope.$broadcast('sequenceInitialized', $scope.selectedSequence);
        }



    }])
    .controller('GraphController', ['$scope', '$http', function ($scope, $http) {
        $scope.sequenceCount;

        $scope.getSequenceByName = function(seqName) {
            var matchingSeq = null;
            for(var i=0; i < $scope.activeSequences.length;i++) {
                if($scope.activeSequences[i].name == seqName)
                {
                    matchingSeq = $scope.activeSequences[i];
                    break;
                }
            }
            return matchingSeq;
        }

        $scope.$on('sequenceInitialized', function(evt, targetSequence) {
           console.log('sequence has been initialized:', $scope.sequenceList, '; target=', targetSequence);
            $scope.sequenceCount = getObjectPropertyCount($scope.sequenceList);
           var container = document.getElementById('graphContainer');
           container.style.height = ($scope.sequenceCount * sequenceRowHeight) + 'px';
//           container.style.width = '480px';
           $scope.createGraph();
        });

        var sequenceRowHeight = 180;
        var inset = 16;
        var leftInset = inset;
        var COLOURS = [ '#E3EB64', '#A7EBCA', '#FFFFFF', '#D8EBA7', '#868E80' ];
        var CHARTCOLOURS = { cycle: "red", click: "grey", div: "teal", pulse:"orange", grid:"gainsboro"};
        var radius = 0;

        var graph;

        $scope.createGraph = function() {
            if(graph == null) {
                graph = Sketch.create({
                    container: document.getElementById( 'graphContainer' ),
                    autoclear: false,
                    retina: false,

                    setup: function() {
                        console.log( 'setup', graph.container );
                        graph.setWidthAndHeight();
                    },

                    setWidthAndHeight: function() {
                        this.height = $scope.sequenceCount * sequenceRowHeight;
                        console.log('container width:', graph.container.clientWidth);
                        this.width = graph.container.clientWidth;
                    },

                    drawBaseLine: function(gridColor) {
                        this.beginPath();
                        graph.strokeStyle = gridColor;
                        graph.lineWidth = 1;
                        // draw bottom line
                        this.moveTo(inset, this.height - inset);
                        this.lineTo(this.width - inset, this.height - inset);
                        graph.stroke();
                    },

                    drawComponentGrid: function(seqComponent, gridColor, maxBarHeight, barWidth, drawClosingElement) {
                        if(!barWidth) barWidth = 1;
                        this.beginPath();
                        graph.strokeStyle = gridColor;
                        graph.lineWidth = 1;
                        // draw 'click' grid
                        console.log("sequence component:", seqComponent);
                        var xPos = 0;
                        var barTopPos = inset + ((1.0 - Math.min(maxBarHeight, 1.0)) * (this.height - inset * 2));
                        var my_gradient;
                        for(var i=0; i < seqComponent.length;i++) {
                            xPos = inset + (seqComponent[i].position*(this.width-(inset*2)));
                            if(barWidth==1) {
                                this.moveTo(xPos, barTopPos);
                                this.lineTo(xPos, this.height - inset);
                            } else {
                                my_gradient = this.createLinearGradient(xPos-1, 0, xPos+barWidth, 0);
                                my_gradient.addColorStop(0, gridColor);
                                my_gradient.addColorStop(1, "white");
                                this.fillStyle = my_gradient;
                                this.fillRect(xPos-1, barTopPos, barWidth, (this.height - inset)-barTopPos);
                            }
                        }
                        if(drawClosingElement)
                        {
                            xPos = this.width - inset;
                            this.moveTo(xPos, inset);
                            this.lineTo(xPos, this.height - inset);
                        }
                        graph.stroke();

                    },

                    renderGraph: function() {
                        console.log('renderGraph: ', getObjectPropertyCount($scope.sequenceList), 'sequences.', $scope.sequenceList);
                        console.log('canvas is ',this.width, 'x', this.height);
                        graph.clear();
                        this.textBaseline = "top";
                        var index = 0;
                        for(var s in $scope.sequenceList) {
                            console.log('will render sequence \'' + s + '\'');
                            this.save();
                            this.translate(0, index * sequenceRowHeight);
                            graph.drawBaseLine(CHARTCOLOURS.grid);
                            graph.drawComponentGrid($scope.sequenceList[s].click, CHARTCOLOURS.grid, 1.0, 1, true);
                            graph.drawComponentGrid($scope.sequenceList[s].cycle, CHARTCOLOURS.cycle, 0.85);
                            graph.drawComponentGrid($scope.sequenceList[s].div, CHARTCOLOURS.div, 0.6667);
                            graph.drawComponentGrid($scope.sequenceList[s].pulse, CHARTCOLOURS.pulse, 0.3333, 16);
                            graph.font = "normal 18px sans-serif";
                            graph.fillStyle = CHARTCOLOURS.grid;
                            var seqRec = $scope.getSequenceByName(s);
                            this.fillText(s + ': ' + seqRec.pulses + '/' + seqRec.split, leftInset * 2,inset);
                            this.restore();
                            index++;
                        }
                    },

                    update: function() {
                        radius = 2 + Math.abs( Math.sin( graph.millis * 0.003 ) * 50 );
                    },

                    resize: function() {
                        console.log('graph:', graph);
                        if(graph) {
                            graph.setWidthAndHeight();
                            graph.renderGraph();
                        }
                    },

                    // Event handlers

                    keydown: function() {
                        if ( graph.keys.C ) graph.clear();
                    },

                    // Mouse & touch events are merged, so handling touch events by default
                    // and powering sketches using the touches array is recommended for easy
                    // scalability. If you only need to handle the mouse / desktop browsers,
                    // use the 0th touch element and you get wider device support for free.
                    touchmove: function( e ) {

        //                for ( var i = graph.touches.length - 1, touch; i >= 0; i-- ) {
        //
        //                    touch = graph.touches[i];
        //
        //                    graph.lineCap = 'round';
        //                    graph.lineJoin = 'round';
        //                    graph.fillStyle = graph.strokeStyle = COLOURS[ i % COLOURS.length ];
        //                    graph.lineWidth = radius;
        //
        //                    graph.beginPath();
        //                    graph.moveTo( touch.ox, touch.oy );
        //                    graph.lineTo( touch.x, touch.y );
        //                    graph.stroke();
        //                }
                    }
                });

                graph.start();
            }
            graph.renderGraph();
        }

    }])
    ;

   function getObjectPropertyCount(foo) {
       var count = 0;
       for (var k in foo) {
           if (foo.hasOwnProperty(k)) {
               ++count;
           }
       }
       return count;
   }
