angular.module('asymmetric_beats', ['graphic_analysis']).
    config(function($routeProvider) {
        $routeProvider.
            when('/', {controller:HomeCtrl, templateUrl:'home.html',resolve: HomeCtrl.resolve}).
            when('/graphic/', {controller:HomeCtrl, templateUrl:'content/graphic_analysis.html',resolve: HomeCtrl.resolve}).
            when('/edit/:projectId', {controller:EditCtrl, templateUrl:'detail.html'}).
            when('/new', {controller:CreateCtrl, templateUrl:'detail.html'}).
            otherwise({redirectTo:'/'});
    })
;

function HomeCtrl($scope, $http) {
    console.log('initing HomeCtrl controller');
}


function CreateCtrl($scope, $location, Project) {
    $scope.save = function() {
        Project.save($scope.project, function(project) {
            $location.path('/edit/' + project._id.$oid);
        });
    }
}


function EditCtrl($scope, $location, $routeParams, Project) {
    var self = this;

    Project.get({id: $routeParams.projectId}, function(project) {
        self.original = project;
        $scope.project = new Project(self.original);
    });

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.project);
    }

    $scope.destroy = function() {
        self.original.destroy(function() {
            $location.path('/list');
        });
    };

    $scope.save = function() {
        $scope.project.update(function() {
            $location.path('/');
        });
    };
}