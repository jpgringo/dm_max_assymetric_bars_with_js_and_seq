autowatch = 1;

this.outlets = 2;

var MAIN_SEQ_NAME = "mSeq";
if(this.post == undefined) { this.post = function(msg) { console.log(msg)}; }

var main_seq = this.patcher != undefined ? this.patcher.getnamed('main_seq') : new SequenceObject();
post("main_seq=" + main_seq);

function test_input(input) {
    post("input: " + input + " [" + (typeof input) + "]\n");
}

function create_basic_seq(totalPulses, sequenceGroup, sequenceName) {
    post("will create basic seq. totalPulses=" + totalPulses + '; sequenceGroup=' + sequenceGroup + '; sequenceName=' + sequenceName + '\n');
    sequenceName = sequenceName ? sequenceName : MAIN_SEQ_NAME;
    main_seq.seq(sequenceName);
    main_seq.clear(sequenceName);
    main_seq.add(sequenceName, 0.0, 'cycle');
    main_seq.add(sequenceName, 0.0, 'div');
    for(var i=0; i < totalPulses; i++)
    {
        post(i);
        main_seq.add(sequenceName, i * (1.0/totalPulses), 'click');
        main_seq.add(sequenceName, i * (1.0/totalPulses), 'pulse');
    }
    main_seq.play(1);
    main_seq.dump(sequenceName);
}

function create_canonic_reversal_pattern(totalPulses, splitPoint, sequenceGroup, sequenceName) {
    if(!sequenceGroup) {
        sequenceGroup = 'main_seq';
    }
    else if(sequenceGroup != 'main_seq')
    {
        post('ERROR!! multiple sequence groups are not supported!!');
        return;
    }
    if(splitPoint == 0) {
        create_basic_seq(totalPulses, sequenceGroup, sequenceName);
        return;
    }
    sequenceName = sequenceName ? sequenceName : MAIN_SEQ_NAME;
    post('will create canonic reversal pattern for sequence \'' + sequenceName + '\' based on ' + totalPulses + " total pulses, split at " + splitPoint + '\n');
    var pulseGroups = [totalPulses - splitPoint, splitPoint];
    var groupStarts = [0.0, splitPoint / totalPulses];
    post("pulse groups:" + pulseGroups + "\n");
    post("group starts:" + groupStarts + "\n");


    // build the sequence
    main_seq.seq(sequenceName);
    main_seq.clear(sequenceName);
    main_seq.add(sequenceName, 0.0, 'cycle');
    var groupLen, groupPulseWidth;
    for(var i=0; i < pulseGroups.length; i++) {
        main_seq.add(sequenceName, groupStarts[i], 'div');
        groupLen  = (i == pulseGroups.length - 1 ? 1.0 : groupStarts[i+1])
            - (i == 0 ? 0.0 : groupStarts[i]);
        groupPulseWidth = groupLen / pulseGroups[i];
        post("group [" + i + "]: len=" + groupLen + ';width=' + groupPulseWidth + '\n');
        for(var j=0; j < pulseGroups[i]; j++) {
            main_seq.add(sequenceName, groupStarts[i] + j*groupPulseWidth, 'pulse');
        }
    }
    // build the click
    var clickWidth = 1.0 / totalPulses;
    for(var i=0; i < totalPulses; i++) {
        main_seq.add(sequenceName,i*clickWidth, 'click');
    }
    main_seq.play(1);
    main_seq.dump(sequenceName);
    return main_seq;
}

function hello_world() {
    post("hello world\n");
}

// EVERYTHING BELOW HERE IS FOR THE WEB ANALYSIS TOOL, AND SO DOESN'T NEED TO ACCOMMODATE MAX/MSP
function SequenceObject() {
    var sequences = new Object();
    var activeSequence = null;
    this.seq = function(seqName) {
        console.log('will select / create sequence:', seqName);
        if(!sequences[seqName]) {
            sequences[seqName] = new Array();
        }
        activeSequence = sequences[seqName];
        console.log('active sequence:', activeSequence);
    };

    this.clear = function(seqName) {
        console.log('will perform clear operation');
        if(seqName == 'all')
        {
            for(var s in sequences) {
                sequences[s] = new Array();
            }
        } else {
            sequences[seqName] = new Array();
        }
    }

    this.add = function(seqName, position, eventName) {
      console.log('add item to seq ', seqName, ' at position ', position);
      var sequence = sequences[seqName];
      if(sequence) {
        var extraArgs = [];
          if(arguments.length > 3) {
              for(var i=3; i < arguments.length; i++) extraArgs.push(arguments[i]);
          }
        var seqEvent = { position: position, name: eventName, args: extraArgs.join(' ') };
        sequence.push(seqEvent);
      }
    };

    this.play = function() {
        // just a stub, this is currently meaningless in this context
    }

    this.dump = function() {
        // TODO: create the sequence object dump function
        console.log("sequence:", sequences);
        var organizedSequences = new Object();
        for(var s in sequences) {
            organizedSequences[s] = {cycle:[], click:[],div:[],pulse:[]};
            for(var i=0;i < sequences[s].length; i++) {
                organizedSequences[s][sequences[s][i].name].push({position:sequences[s][i].position, args:sequences[s][i].args});
            }
        }
        return organizedSequences;
    }

    this.toString = function() {
        return "how very awful for you";
    }
}
