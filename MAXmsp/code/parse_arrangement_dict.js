autowatch = 1;
outlets = 2;

function parse_orchestration(section) {
    var orchestration = section.get('orchestration');
    post('orchestration:' + orchestration + '\n');
    var orchestrationKeys = orchestration.getkeys();
    if((typeof orchestrationKeys) == 'string') { orchestrationKeys = [orchestrationKeys]};
    post('orchestrationKeys:' + orchestrationKeys + " [" + (typeof orchestrationKeys) + ']\n');
    var orchestrationParams;
    for (var i = 0; i < orchestrationKeys.length; i++) {
        orchestrationParams = orchestration.get(orchestrationKeys[i]);
        post(orchestrationKeys[i] + ': ' + orchestrationParams + '\n');
        post("orchestrationParams:" + (typeof orchestrationParams) + '\n');
        var paramKeys = orchestrationParams.getkeys();
        if ((typeof paramKeys) == 'string') { // for some reason Max turns a single-item array containing a string into a simple string primitive
            post('paramKey=' + paramKeys + '\n');
            outlet(0, [orchestrationKeys[i], 'set' + paramKeys, orchestrationParams.get(paramKeys) ]);
        } else {
            for (var j = 0; j < paramKeys.length; j++) {
                post('p=' + paramKeys[j] + '\n');
                outlet(0, [orchestrationKeys[i], 'set' + paramKeys[j], orchestrationParams.get(paramKeys[j])]);
            }
        }
    }
}

function parse_pattern(section) {
    var pattern = section.get('pattern');
    if(!pattern) return;
    post('pattern:' + pattern + '\n');
    var patternKeys = pattern.getkeys();
    post('patternKeys: ' + patternKeys + ' [' + (typeof patternKeys) + ']\n');
    outlet(1, ['create_canonic_reversal_pattern', pattern.get('beats'), pattern.get('split')]);
}

function load_arrangement(dictName, sectionName)
{
	post('dictName:' + dictName + '; sectionName: ' + sectionName + '\n');
	var arrDict = new Dict(dictName);
    var section = arrDict.get(sectionName);
    post('section:' + section + '\n');

    // handle the pattern
    parse_pattern(section);
    // handle the orchestration
    parse_orchestration(section);
}

