{
	"name" : "assymetric bars with js and seq~",
	"version" : 1,
	"creationdate" : 3449853046,
	"modificationdate" : 3450586825,
	"viewrect" : [ 26.0, 113.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"assymetric_bars.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"sequence_generator.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"note_generator.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"multiple sequence test.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"code" : 		{
			"asym_seq_generator.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0
}
